# challenge-ml
Aplicación de línea de comandos que permite obtener todos los mails de una cuenta de google que contengan la palabra clave "DevOps" (case insensitive). Excluyendo los que se encuentren en la papelera y los mensajes de spam.

## Requisitos

Para correr la aplicación es necesario tener instalado python 3.8

### Pasos a seguir en Ubuntu

- Clonar el repositorio:
```bash
$ git clone https://mliezun@bitbucket.org/mliezun/challenge-ml.git
$ cd challenge-ml
```

- Instalación de prerequisitos:
```bash
$ sudo apt update
$ sudo apt install software-properties-common
```

- Agregar PPA deadsnakes:
```bash
$ sudo add-apt-repository ppa:deadsnakes/ppa
```

- Instalar python 3.8
```bash
$ sudo apt install python3.8 python3.8-venv
```

- Crear un entorno virtual e instalar dependencias
```bash
$ python3.8 -m venv myenv
$ source myenv/bin/activate
$ pip install -r requirements.txt
```

## ¿Cómo usar?

Para correr la aplicación se debe ejecutar el siguiente comando desde la consola:
```bash
$ python main.py
```

La primera vez que se ejecute se abrirá una ventana de navegador, pidiendo permiso para que la aplicación pueda leer los mensajes de gmail. Se debe otorgarle el permiso por más que diga que es una aplicación no verificada (la verificación de aplicaciones de escritorio puede tardar varios meses).

Si el permiso fue otorgado, la aplicación obtendrá todos los mensajes que contengan la palabra "DevOps" sin discriminar por mayúsculas o minúsculas. Luego los almacenará en una base de datos sqlite llamada: `messages.db`.

Si se vuelve a ejecutar no se volverán a traer todos los mensajes sino sólo aquellos que sean posteriores a 1 día anterior a la máxima fecha que esté almacenada en la base de datos.


#### Obtener el listado de mensajes

Ejecutar desde la consola:

```bash
$ python main.py list
```

Output:
```
date                    from                                               subject
2020-05-13 17:19:59     "Fátima Leotta" <fatima.leotta@mercadolibre.com>   Challenge - Mercado Libre
...
```

#### Acceder a los datos
Para acceder a la base de datos sqlite se puede usar un pograma llamado [sqlitebrowser](https://sqlitebrowser.org/).

Otra forma de acceder es a través de la línea de comandos.
```bash
$ sqlite3 messages.db
> SELECT DATETIME(ROUND(date / 1000), 'unixepoch') fecha, `from`, subject FROM messages;
```
