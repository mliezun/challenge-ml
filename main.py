import pickle
import sqlite3
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from threading import Thread
import datetime
from tqdm import tqdm
import sys

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']

# Quickstart code for gmail api
# https://developers.google.com/gmail/api/quickstart/python#step_3_set_up_the_sample


class Gmail:
    def __init__(self):
        """Shows basic usage of the Gmail API.
        Lists the user's Gmail labels.
        """
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        self.creds = creds

    def getService(self):
        return build('gmail', 'v1', credentials=self.creds)

    def storeMessages(self, db, q, **kwargs):
        threads = []

        max_date = db.execute(
            'SELECT MAX(date) FROM messages;').fetchall()[0][0]

        log_start = "Retrieving list of messages."

        if max_date:
            date_after = (datetime.datetime.fromtimestamp(
                max_date/1000) - datetime.timedelta(days=1))
            after = date_after.strftime('%Y/%m/%d')
            q += ' after:' + after
            log_start += " After %s." % (after,)

        print(log_start)

        # Call the Gmail API
        user = dict(userId='me')
        args = dict(q=q, **user, **kwargs)
        results = self.getService().users().messages().list(**args).execute()

        rows = []

        for m in results.get('messages'):
            def retrieve_msg(self, m):
                service = self.getService()
                msg = service.users().messages().get(
                    id=m.get('id'), **user, format="metadata").execute()
                headers = msg.get('payload').get('headers')
                msg_data = dict()
                for h in headers:
                    msg_data[h['name']] = h['value']
                values = (m.get('id'), msg.get('internalDate'),
                          msg_data['From'], msg_data['Subject'])
                rows.append(values)
            t = Thread(target=retrieve_msg, args=(self, m))
            threads.append(t)
            t.start()

        for t in tqdm(threads, desc="Obtaining msg info", unit="msg"):
            t.join()

        db.executemany(
            'INSERT OR IGNORE INTO messages VALUES (?, ?, ?, ?)', rows)
        db.commit()

        print("%d %s saved" % (total := len(threads),
                               "messages" if total != 1 else "message"))

        return db


def main(args):
    with sqlite3.connect('messages.db') as db:
        db.execute("""
        CREATE TABLE IF NOT EXISTS messages (
            id text primary key,
            date number not null,
            `from` text not null,
            subject text not null
        );
        """)
        if len(args) == 1:
            gmail = Gmail()
            gmail.storeMessages(db, q="DevOps")
        elif len(args) == 2 and args[1] == 'list':
            print('date\t\t\tfrom\t\t\t\t\tsubject')
            for row in db.execute("SELECT DATETIME(ROUND(date / 1000), 'unixepoch'), `from`, subject FROM messages;").fetchall():
                out = ""
                for c in row:
                    out += str(c) + "\t"
                print(out)
        else:
            print('Usage: main.py [list]')
            exit(1)


if __name__ == '__main__':
    main(sys.argv)
